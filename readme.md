## theater automation command line app

This app is intended to hide inadvertent changes to projected images during color correction sessions.  It listens to the SDI stream and watches router changes.  If SDI listener detects a change to the signal format the app will send a couple commands to the projector.  Command 1 closes the dowser so the image is hidden, command 2 recalls the last selected projecor preset so the image is restored.  These commands will only be sent when the 'auto enable' command is entered.

### prerequisites

Workstation requirements
1. Ubuntu 20.04 or MacOS Monterey
2. NodeJS > 18
3. Blackmagic Decklink SDI input device and Desktop Video driver installed
4. Ultragrid 1.8

If you're running this app on Ubuntu, you'll need to compile Ultragrid. See compilation instructions [here...][def]
For MacOS, just download and install [here...][def2]


### Howto
Make sure you're in the root of the project

Install NodeJS modules
```
npm install
```

Edit config.js file to set IP addresses for your Barco projector and Blackmagic Devices SDI router.

Start the program:
```
node index
```

Once you've established a connection to the BMD router you can run the 'router' command to see all the input and output labels.  
Find the router output feeding yout projector SDI input and set that as your `rtr.watch` variable in the `config.js` file.

You'll need to wire one of your routers SDI outputs to the Blackmagic SDI input device. 
Set that router output number as 'preview' `rtr.preview` in the `config.js` file.

Run the 'help' command for a list of commands and what they do.
Run the 'status command for app status


[def]: https://github.com/CESNET/UltraGrid/wiki/Compile-UltraGrid-(Source)
[def2]: http://www.ultragrid.cz/installation-and-running/