module.exports = {
    projectorIP: '10.208.79.48',
    routerIP: '10.208.79.51',
    rtr: {
        watch: 7,
        preview: 15
    },
    webserver: '10.208.79.177',
    tcpPort: 8144
}
