const net = require('node:net')
const config = require('./config')
const ipc = require('./listen')

let port = config.tcpPort

const server = net.createServer((socket) => {

    socket.on('data', (data) => {
        let stringData = data.toString()
        console.log(`tcp server data: ${data.toString()}`)

        switch (stringData) {
            case 'init':
                console.log('connect init...')
                ipc.emit('get appstatus')
                break;
            case 'lock':
                console.log('turn preset lock on')
                ipc.emit('automation', true)
                break;
            case 'unlock':
                console.log('turn preset lock off')
                ipc.emit('automation', false)
                break;
            case 'test':
                console.log('test packet received')        
                break;
            default:
                console.log(`unrecognized data: ${stringData}`)
                break;
        }
    })

    socket.on('end', () => {
        console.log('tcp server: connection ended')
    })

    socket.on('error', (err) => {
        console.log(`tcp server socket error: ${err.message}`)
    })

    ipc.on('tcp setstate', (msg) => {
        // console.log(`tcp setstate: ${msg}`);
        if (msg === false) {
            socket.write('0_unlock')
        }
        
        if (msg === true) {
            socket.write('1_lock')
        }
    })

    ipc.on('appStatus', (msg) => {
        // console.log(`tcp setstate: ${msg}`);
        if (msg === false) {
            socket.write('0_unlock')
        }
        
        if (msg === true) {
            socket.write('1_lock')
        }
    })
    
    ipc.on('SDI range', (msg) => {
        console.log(`tcp server: sdi range ${msg}`);
        msg ? socket.write('2_full') : socket.write('2_legal') 
    })

    ipc.on('SDI format', (data) => {
        let formatString = data.toString()
        socket.write(`3_${formatString}`)
    })

    ipc.on('tcp sendMacro', (msg) => {
        if (msg != null) {
            let macroString = msg.toString()
            socket.write(`4_${macroString}`)
        }
    })
})

server.on('error', (err) => {
    console.log(`tcp server error: ${err.message}`)
    ipc.emit('tcp server', false)
})

server.listen(port, () => {
    console.log(`TCP socker server listening on port: ${port}`)
    ipc.emit('tcp server', true)
})