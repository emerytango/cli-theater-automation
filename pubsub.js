const fs = require('node:fs')
const clc = require('cli-color')
const ipc = require('./listen')
const CMD = require('./commands/barcoTCP')
const net = require('node:net')
const config = require('./config')
const timeStamp = require('./timestamp')
const host = config.projectorIP
const port = 43728
const barcoStatus = {

}
const commandSuccess = Buffer.from(CMD.success)
const ACK = Buffer.from(CMD.ACK)


ipc.on('pubsub', (msg) => {
    ipc.emit('server messages', `ipc init: projector ip: ${host}`)
})

ipc.on('barco command', (msg) => {
    // console.log(clc.cyan(`projector command: ${msg}`))
    switch (msg) {
        case 'dowser':
            barcoComm(CMD.shutterClose)
            break;
        case 'macro':
            barcoComm(CMD.lastMac)
            break;
        case 'writeMacro':
            if (barcoStatus.lastMacro) {
                let decArr = string2ascii(barcoStatus.lastMacro)
                let chkSUM = chksumCalc(decArr)
                let endPacket = [0, chkSUM, 255]
                let buildCMD = CMD.macroHeader.concat(decArr, endPacket)
                barcoComm(buildCMD)
            }
            break;
        case 'sigint':
            let decArr = string2ascii('checkSignal')
            let chkSUM = chksumCalc(decArr)
            let endPacket = [0, chkSUM, 255]
            let buildCMD = CMD.macroHeader.concat(decArr, endPacket)
            barcoComm(buildCMD)
            break;
        default:
            break;
    }

})


function barcoComm(command) {
    const projector = net.createConnection({
        host: host,
        port: port
    })

    projector.on('connect', () => {
        projector.write(Buffer.from(command))
    })

    projector.on('data', (data) => {
        // if (data.equals(commandSuccess) === true) console.log(clc.green(`projector: cmd success`))
        // if (data.equals(ACK) === true) console.log(clc.green(`projector: ACK`))
        if (data[2] === 0xe8) {
            let i = data.length - 3
            barcoStatus.lastMacro = data.toString('ascii', 4, i)
            if (barcoStatus.lastMacro !== 'checkSignal') {
                ipc.emit('barco status', barcoStatus.lastMacro)
            }
        }
    })


    projector.on('end', () => {
        ipc.emit('prompt')
    })

    projector.on('error', (err) => {
        console.log(clc.red(`${timeStamp()} -- projector error: ${err}`));
    })

    setTimeout(() => {
        projector.end()
    }, 5000);

}

let i, total, cmdSum

function string2ascii(str) {
    let arr = [];
    let len = str.length;
    for (i = 0; i < len; i++) {
        arr[i] = str.charCodeAt(i)
    };
    return arr;
}

function chksumCalc(input) {
    cmdSum = 361
    total = 0;
    for (i = 0; i < input.length; i++) {
        total += input[i];
    }
    return (cmdSum + total) % 256;
}


setInterval(() => {
    barcoComm(CMD.lastMac)
}, 15000);