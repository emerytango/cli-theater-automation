const timeStamp = function () {   
    let date = new Date()
    let year = date.getFullYear();
    let month = addZero((date.getMonth() + 1).toString())
    let day = addZero(date.getDate().toString())
    let hour = addZero(date.getHours().toString())
    let minute = addZero(date.getMinutes().toString())
    let second = addZero(date.getSeconds().toString())
    return `${year}${month}${day}-${hour}:${minute}:${second}`
}

function addZero(n) {
    return n<10 ? '0'+n : n
}

module.exports = timeStamp