const readline = require('node:readline')
// const net = require('node:net')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: ':-co3-: '
})
const clc = require('cli-color')
const config = require('./config')
const ipc = require('./listen')
const bridge = require('./tcpServer')
const timeStamp = require('./timestamp')
require('./bmdRouter')
require('./pubsub')
const help = require('./commands/help')
let appStatus = {
    projectorIP: config.projectorIP,
    routerIP: config.routerIP,
    routerWatch: config.rtr,
    uvStatus: false,
    auto: false,
    lastMacro: null
}


ipc.on('ping', () => {
    ipc.emit('pubsub', 'projector module')
    rl.prompt()
})


ipc.on('server messages', (msg) => {
    console.log(clc.yellowBright(`${timeStamp()}  ${msg}`))
    setTimeout(() => {
        rl.prompt()
    }, 500);
})


ipc.on('uv', (msg) => {
    appStatus.uvStatus = msg
    if (appStatus.uvStatus) {
        console.log(clc.yellowBright('SDI listener running'))
        rl.prompt()
    } else {
        console.log(clc.yellowBright('SDI listener stopped'))
        rl.prompt()
    }
})

ipc.on('view', (msg) => {
    appStatus.viewer = msg
    console.log(clc.yellowBright(`viewer: ${appStatus.viewer}`))
    rl.prompt()
})

ipc.on('barco status', (msg) => {
    if (appStatus.lastMacro === null || appStatus.lastMacro === 'refresh') {
        console.log(clc.magenta('\n...current projector macro...'));
        process.stdout.write(clc.green(timeStamp() + '   '))
        console.log(clc.green.underline(msg));
        appStatus.lastMacro = msg
        rl.prompt()
    } else {
        let macroCheck = msg
        if (macroCheck !== appStatus.lastMacro) {
            console.log(clc.magenta('\n...macro changed by a user...'))
            console.log(clc.yellow(`  old Macro: ${appStatus.lastMacro}`))
            appStatus.lastMacro = msg
            process.stdout.write(clc.green(timeStamp() + '   '))
            console.log((clc.green.underline(`new macro: ${msg}`)))
            rl.prompt()
        }
    }
})

ipc.on('SDI range', (msg) => {
    console.log(`index js: sdi range ${msg}`);
    appStatus.fullRange = msg
    if (appStatus.auto) {
        process.stdout.write(clc.green(timeStamp() + '   '))
        console.log(clc.red('automation enabled... sending test pattern macro'))
        // console.log(clc.blueBright(appStatus.lastMacro))
        setTimeout(() => {
            ipc.emit('barco command', 'sigint')
        }, 500);
    }
})

ipc.on('SDI format', (msg) => {
    appStatus.sdiFormat = msg
    console.log(clc.cyan(`${timeStamp()}  SDI format change: ${appStatus.sdiFormat}`))
    rl.prompt()
})

ipc.on('quit app', () => {
    console.log(clc.yellow('shutting down'))
    setTimeout(() => {
        rl.close()
    }, 2000);
})

ipc.on('prompt', () => {
    rl.prompt()
})

ipc.on('rtr app status', (msg) => {
    appStatus.routerStatus = msg
})

ipc.on('router stat', (msg) => {
    console.log(msg.ioTable)
    console.log(msg.inputLabels)
    console.log(msg.outputLabels)
})

ipc.on('automation', (msg) => {
    console.log(clc.magenta(`automation request from tcp bridge: ${msg}`))
    msg === true ? autoEnable() : autoDisable()
})

ipc.on('tcp server', (msg) => {
    console.log(clc.green(`tcp server running: ${true}`))
    appStatus.tcpServer = true
})

ipc.on('get appstatus', () => {
    console.log(clc.green('web client init request'))
    ipc.emit('tcp setstate', appStatus.auto)
    setTimeout(() => {
        ipc.emit('tcp sendMacro', appStatus.lastMacro)
    }, 500)
    // if (appStatus.sdiFormat) {
    //     setTimeout(() => {
    //         ipc.emit('SDI format', appStatus.sdiFormat)
    //     }, 1000)
    // }
    // if (appStatus.fullRange) {
    //     setTimeout(() => {
    //         ipc.emit('SDI range', appStatus.fullRange)
    //     }, 1500)
    // }
})


rl.on('line', (line) => {

    switch (line) {
        case 'status':
            console.log(appStatus)
            break;
        case 'help':
            console.log(help)
            break;
        case 'exit':
            if (appStatus.uvStatus === true) {
                ipc.emit('stop uv', 'exit')
            } else {
                rl.close()
            }
            break;
        case 'stop':
            if (appStatus.uvStatus) {
                ipc.emit('stop uv')
            } else {
                console.log(clc.yellow('uv not running, nothing to do...'))
            }
            break;
        case 'router':
            ipc.emit('router info')
            break;
        case 'refresh':
            appStatus.lastMacro = 'refresh'
            ipc.emit('barco command', 'macro')
            break;
        case 'auto enable':
            autoEnable()
            break;
        case 'auto disable':
            autoDisable()
            break;
        case 'listen':
            if (!appStatus.uvStatus) {
                ipc.emit('listen')
            } else {
                console.log(clc.yellow('uv already running...'))
            }
            break;
        case 'preview':
            if (appStatus.uvStatus) {
                ipc.emit('preview')
            } else {
                ipc.emit('listen')
                ipc.emit('preview')
            }
            break;
        case '':
            rl.prompt()
            break
        default:
            console.log(clc.yellow(`unrecognized command: ${line}`));
            break;
    }
    rl.prompt()
}).on('close', () => {
    console.log(clc.greenBright('have a nice day'));
    process.exit(0)
})

function autoEnable() {
    if (appStatus.uvStatus) {
        console.log(clc.green(`projector automation enabled, getting last macro...`))
        ipc.emit('barco command', 'macro')
    } else {
        console.log(clc.yellow('SDI listener not running... starting listener'));
        ipc.emit('listen')
        setTimeout(() => {
            ipc.emit('barco command', 'macro')
        }, 2000)
    }
    appStatus.auto = true
    ipc.emit('appStatus', true)
}

function autoDisable() {
    console.log(clc.red(`projector automation disabled`))
    appStatus.auto = false
    ipc.emit('appStatus', false)
}


// t1 cli-auto 10.208.79.115