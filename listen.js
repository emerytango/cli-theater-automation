const os = require('os')
const EventEmitter = require('node:events')
class MyEmitter extends EventEmitter { }
const ipc = new MyEmitter()
module.exports = ipc
const { spawn } = require('node:child_process')
const clc = require('cli-color')
const timeStamp = require('./timestamp')
let streamSDI
let viewStream
let viewer
const formatRegex = /R10k/
const videoRegex = /UYVY/
const encoderRegex = /\[lavc\]/
let inputRegex = /video\ input/
const platform = os.platform()
const release = os.release()


setTimeout(() => {
    ipc.emit('ping', 'ipc init')
}, 1000)


ipc.on('listen', () => {
    // console.log('starting ultragrid...')
    if (platform === 'darwin') {
        startUV('/Applications/uv-qt.app/Contents/MacOS/uv')
    } else {
        startUV('/usr/local/bin/uv')
    }
})

ipc.on('preview', () => {
    // ipc.emit('server messages', 'starting preview')
    if (platform === 'darwin') {
        startViewer('/Applications/uv-qt.app/Contents/MacOS/uv')
    } else {
        startViewer('/usr/local/bin/uv')
    }
})

function startUV(a) {
    streamSDI = spawn(a, [
        '--verbose=3',
        '-t',
        'decklink',
        '-s',
        'embedded',
        '-c',
        'libavcodec:codec=MJPEG',
        '-P',
        '5218'
    ])
    
    if (streamSDI.pid) {
        ipc.emit('uv', true)
        console.log(clc.green(`ultragrid PID: ${streamSDI.pid}`))
    }
    
    streamSDI.stdout.on('data', (data) => {
        
        if (videoRegex.test(data)) {
            let fullRange
            // UYVY -- check for R10k, if yes, full range
            if (formatRegex.test(data)) {
                fullRange = true
                process.stdout.write(clc.yellowBright(timeStamp()))
                console.log(clc.green('  SDI stream is full range'))
            } else {
                fullRange = false
                process.stdout.write(clc.yellowBright(timeStamp()))
                console.log(clc.green('  SDI stream is legal range'));
            }
            ipc.emit('SDI range', fullRange)
        }

        if (inputRegex.test(data)) {
            let filt = inputRegex.exec(data)
            let formatIndex = filt.input.substring(filt.input.indexOf('t:') + 3)
            formatIndex = formatIndex.replace('\n', '')
            // process.stdout.write(clc.green(formatIndex))
            ipc.emit('SDI format', formatIndex)
        }

    })

    streamSDI.on('close', () => {
        ipc.emit('uv', false)
    })


    streamSDI.on('exit', (code) => {
        console.log(clc.cyan(`uv exit code ${code}`))
    })

    ipc.on('stop uv', (msg) => {
        // console.log('stopping uv');
        streamSDI.kill()
        if (viewer) viewStream.kill()
        if (msg === 'exit') {
            ipc.emit('quit app')
        }
    })

}


function startViewer(a) {
    viewStream = spawn(a, [
        '--verbose=1',
        '-d',
        'gl:size=50',
        '-p',
        'border:color=#0a2020',
        '--param',
        'window-title=CO3 SDI viewer',
        '-P',
        '5218'
    ])
    
    if (viewStream.pid) {
        viewer = true
        ipc.emit('view', viewer)
    }
   
    viewStream.on('close', () => {
        viewer = false
        ipc.emit('view', viewer)
    })

    viewStream.on('exit', (code) => {
        viewer = false
        ipc.emit('server messages', `preview exit code: ${code}`)
    })

}
