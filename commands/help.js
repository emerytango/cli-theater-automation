const help = `
commands:
    help            print this
    status          print status of all modules
    listen          start SDI stream listener
    preview         open the SDI preview window
    stop            stop SDI stream listener
    auto enable     turn signal change automation on
    auto disable    turn signal change automation off
    refresh         read (update) the projector preset in use
    router          get router labels and io info
    exit            quit the app

Notes:
    'listen' will start the SDI listener.  The console will show timestampted
    notifications when the listener detects changes to the SDI stream

    'preview' will open a viewer window to show a live preview of the SDI stream

    Assuming the watch and preview variables have been set correctly in the config
    file, any changes to the router output wired to the projector SDI input will 
    automatically be applied to the preview SDI input. 

    'router' will show a list of current router io, router inputs, outputs, and their 
    labels.  Use this information to get the router outputs associated with the 
    projector input and SDI preview input.

    'auto enable' command will start the SDI listener (if it's not already running) 
    and get the last macro selected by the user.  Any router changes to the 
    destination feeding the projector will result in a dowser close command send on 
    the projector.  Any changes to the SDI stream will result in resending the last
    macro to the projector. 

    The application will query the projector every 15 seconds.  The console will log
    when the projector preset has been changed by the user.
    
    'stop' will kill the listener and preview window (if it's running.)

`
module.exports = help