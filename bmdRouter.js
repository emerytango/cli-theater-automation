const ipc = require('./listen')
const net = require('node:net')
const clc = require('cli-color')
const config = require('./config')
let ipaddr = config.routerIP
let routerWatch = config.rtr.watch
let isOnline = false
let ioTable = {}
let inputLabels = {}
let outputLabels = {}
let ioRegex = /(^[0-9]{1,2})\s([0-9]{1,2})/
let labelRegex = /(^[0-9]{1,2})\s(.{3,})/
let chunk = ''
let getData = 'io'
let auto = false

// green, cyan

const bmdRouter = net.createConnection({
	port: 9990,
	host: ipaddr
}, () => {
	console.log(`connecting to router at ${ipaddr}`)
})

ipc.on('appStatus', (msg) => {
	auto = msg
})

bmdRouter.on('connect', () => {
	isOnline = true
	ipc.emit('server messages', `BMD router at IP ${ipaddr} is connected`)
	ipc.emit('rtr app status', isOnline)
	getIO()
})

bmdRouter.on('data', (data) => {
	// console.log(clc.cyan(data.toString()))
	if (data.length < 30 && data.toString() != 'ACK') {
		var rtrChange = data.slice(data.indexOf(0x0a),)
		var parsed = /([0-9]{1,2})\s([0-9]{1,2})/.exec(rtrChange.toString())
		if (parsed !== null) {
			console.log(clc.cyan(`io change: destination ${parsed[1]} -- source ${parsed[2]} `))
			if (parsed[1] == routerWatch) {
				console.log(clc.cyanBright(`SDI route change... setting preview to match`))
				bmdRouter.write(Buffer.from('VIDEO OUTPUT ROUTING:\n'))
				bmdRouter.write(Buffer.from(`15 ${parsed[2]}`))
				bmdRouter.write(Buffer.from('\n\n'))
				if (auto) {
					console.log(clc.red('automation is on, sending close dowser to projector'))
					ipc.emit('barco command', 'dowser')
					ipc.emit('server messages', 'automation closed dowser')
				}
			}
		}
	}

	chunk += data.toString()
	var index = chunk.indexOf('\n')
	while (index > -1) {
		var line = chunk.substring(0, index)
		parseData(line)
		chunk = chunk.substring(index + 1)
		index = chunk.indexOf('\n')
	}
	chunk = ''
})


bmdRouter.on('close', () => {
	isOnline = false
	console.log('router connection closed')
	ipc.emit('rtr app status', isOnline)
})


bmdRouter.on('end', () => {
	isOnline = false
	console.log('router connection ended')
	ipc.emit('rtr app status', isOnline)
})


bmdRouter.on('error', () => {
	isOnline = false
	console.log('router connection error')
	ipc.emit('rtr app status', isOnline)
})


setTimeout(() => {
	getInputLabels()
}, 2000)

setTimeout(() => {
	getOutputLabels()
}, 3000)

function parseData(z) {
	var ioarr = ioRegex.exec(z)
	var labelArr = labelRegex.exec(z)
	if (getData === "io" && ioarr !== null) ioTable[ioarr[1]] = ioarr[2]
	if (getData === "input" && labelArr !== null) inputLabels[labelArr[1]] = labelArr[2]
	if (getData === "output" && labelArr !== null) outputLabels[labelArr[1]] = labelArr[2]
}


function getInputLabels() {
	getData = "input"
	bmdRouter.write(Buffer.from('INPUT LABELS:\n'))
	bmdRouter.write(Buffer.from('\n'))
}

function getOutputLabels() {
	getData = "output"
	bmdRouter.write(Buffer.from('OUTPUT LABELS:\n'))
	bmdRouter.write(Buffer.from('\n'))
}

function getIO() {
	getData = "io"
	bmdRouter.write(Buffer.from('VIDEO OUTPUT ROUTING:\n'))
	bmdRouter.write(Buffer.from('\n'))
}


ipc.on('router info', () => {
	ipc.emit('router stat', {
		ioTable: ioTable,
		inputLabels: inputLabels,
		outputLabels: outputLabels
	})
})
